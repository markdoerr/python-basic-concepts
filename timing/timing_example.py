#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: _

*timing with timeit etc.*

:details: :
     s. https: // docs.python.org/3/library/timeit.html

:author:  mark doerr <mark@ismeralda.org> : author

:date: (creation)          20210125
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import sys
import logging
import argparse

import timeit
import hashlib


def test_md5(text: str = "default"):
    text_hash = hashlib.md5(text.encode('UTF-8')).hexdigest()


def test_sha256(text: str = "default"):
    text_hash = hashlib.sha256(text.encode('UTF-8')).hexdigest()


def test_sha512(text: str = "default"):
    text_hash = hashlib.sha256(text.encode('UTF-8')).hexdigest()


def test_sha256_loop(max=1000):
    for text in range(max):
        test_sha256(str(text))


def test_sha512_loop(max=1000):
    for text in range(max):
        test_sha512(str(text))


def parse_command_line():
    """ Looking for command line arguments"""

    description = "argparse skeleton"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s ' + __version__)

    return parser.parse_args()


if __name__ == '__main__':
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(
        format='%(levelname)-4s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    parsed_args = parse_command_line()

    print(
        f"md5 (1000_000x): {timeit.timeit(test_md5, number=1000_000)} seconds")

    print(
        f"SHA256 (100_000x): {timeit.timeit(test_sha256, number=1000_000)} seconds")

    print(
        f"SHA512 (100_000x): {timeit.timeit(test_sha512, number=1000_000)} seconds")

    print(
        f"SHA256 - for loop (1000x): {timeit.timeit(test_sha256_loop, number=1000)} seconds")

    print(
        f"SHA512 - for loop (1000x): {timeit.timeit(test_sha512_loop, number=1000)} seconds")
