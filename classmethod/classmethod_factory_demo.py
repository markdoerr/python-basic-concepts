#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: _

*class method factory example*

:details: :
     source: https://www.programiz.com/python-programming/methods/built-in/classmethod
     see also:
     https://realpython.com/instance-class-and-static-methods-demystified/#class-methods

:author:  mark doerr <mark@ismeralda.org> : author

:date: (creation)          20210125
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import sys
import logging

from datetime import date

# random Person


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, name, birthYear):
        return cls(name, date.today().year - birthYear)

    def display(self):
        print(f"{self.name}'s age is: {str(self.age)}")


person = Person('Douglas', 42)
person.display()

person1 = Person.fromBirthYear('Ben',  1968)
person1.display()

# Interitance of class methods


class PersonInh:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, name, birthYear):
        return cls(name, date.today().year - birthYear)

    @staticmethod
    def fromFathersAge(name, fatherAge, fatherPersonAgeDiff):
        return Person(name, date.today().year - fatherAge + fatherPersonAgeDiff)

    def display(self):
        print(f"{self.name}'s age is: {str(self.age)}")


class Man(PersonInh):
    sex = 'Male'


# class method
man = Man.fromBirthYear('Keith', 1977)
man.display()
print("Keith (classmethod) - is instance of Man:", isinstance(man, Man))  # True

# static method
man1 = Man.fromFathersAge('Adam', 1965, 20)
man.display()
print("Adam (staticmethod) - is instance of Man:",
      isinstance(man1, Man))  # False
