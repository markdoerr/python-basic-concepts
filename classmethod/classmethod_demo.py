# demo of class method


class DemoClass:
    def __init__(self, ):
        """
        docstring
        """
        self.instance_var = "world"

    def demo_print(self):
        """
        docstring
        """
        print("instance method - ", self.instance_var)

    @classmethod
    def demo_print_cls(cls):
        """
        docstring
        """
        print("class method")


if __name__ == "__main__":
    dc = DemoClass()
    dc.demo_print()

    DemoClass.demo_print_cls()
