#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: 

*argparse - basic *

:details: : basic argparse skeleton
         https://docs.python.org/3/library/argparse.html
        https://docs.python.org/3/howto/argparse.html
     

:author:  mark doerr <mark@ismeralda.org> 

:date: (creation)          20210125
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import sys
import logging
import argparse


def parse_command_line():
    """ Looking for command line arguments"""

    description = "argparse skeleton"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-s', '--short-argument', action='store',
                        help='argurment with shortcut')  # required=False,
    parser.add_argument('--long-argument', action='store',
                        default="long-arg-default", help='argument without shortcut, can be abbreviated, like --long')
    parser.add_argument('-c', '--check', action='store_true',
                        help='bool: check something.')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s ' + __version__)

    return parser.parse_args()


if __name__ == '__main__':
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(
        format='%(levelname)-4s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    parsed_args = parse_command_line()

    if parsed_args.long_argument:
        logging.debug(f"long arguemnt: {parsed_args.long_argument}")

    if parsed_args.short_argument:
        logging.debug(f"short arguemnt: {parsed_args.short_argument}")

    if parsed_args.check:
        logging.debug(f"checking: {parsed_args.check}")

    if len(sys.argv) <= 2:
        logging.debug("no arguments provided !")
